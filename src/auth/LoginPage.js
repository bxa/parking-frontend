import {useState} from 'react';
import {Navigate, useLocation, useNavigate} from "react-router-dom";
import {useAuth} from "./AuthProvider";
import {Button, TextField} from "@mui/material";
import Card from "@mui/material/Card";
import CardHeader from "@mui/material/CardHeader";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import axios from "../myaxios"
import CardActions from "@mui/material/CardActions";
import {makeStyles} from "@mui/styles";
import CarRepairIcon from '@mui/icons-material/CarRepair';
import clsx from "clsx";

export const styles = makeStyles(theme => ({
    lineHeight: {lineHeight: 2},
    logo: {
        color: theme.palette.primary.main,
        margin: "10px 0",
        fontSize: 130
    }
}));

const LoginPage = () => {
    let navigate = useNavigate();
    let location = useLocation();
    let auth = useAuth();
    const classes = styles();

    const [phone, setPhone] = useState('');
    const [loading, setLoading] = useState(false);
    const [code, setCode] = useState('');
    const [codeSent, setCodeSent] = useState(false);
    const [sec, setSec] = useState(null);

    let t;
    const sendSMS = () => {
        setLoading(true);
        setSec(60);
        t = setInterval(()=> {
            setSec(sec => {
                if(sec > 1)
                    return sec - 1;
                else {
                    clearInterval(t);
                    return null
                }
            })
        }, 1000);

        axios.post('/login', {mobile: phone}).then(() => {
            setLoading(false);
            setCodeSent(true);
        })
    };

    let from = location.state?.from?.pathname || "/";

    function handleSubmit() {
        setLoading(true);
        auth.signIn({phone, code }).then(r => {
            setLoading(true);
            navigate(from, {replace: true})
        });
    }



    if(!!auth?.user) {
        return <Navigate to="/" state={{ from: location }} replace />
    }


    return (<div className={"p-5 bg-slate-100 h-screen te"}>
        <div className="flex justify-center">
            <CarRepairIcon  className={classes.logo}/>
        </div>
        <Card className={clsx(loading && "loading", "p-3")}>
            <CardHeader title={"Login or sign up"}/>
            <CardContent>
                <Typography variant={"h6"}>Please enter your phone number:</Typography>
                <TextField
                    autoFocus
                    margin="normal"
                    id="name"
                    label="Phone number"
                    type="number"
                    fullWidth
                    value={phone}
                    onChange={(e)=>setPhone(e.target.value)}
                />


                <TextField
                    disabled={!codeSent}
                    margin="normal"
                    id="code"
                    label="Confirmation code"
                    type="number"
                    fullWidth
                    value={code}

                    onChange={(e)=>setCode(e.target.value)}
                    InputProps={{endAdornment: <Button disabled={!phone || !!sec} onClick={sendSMS} size={"small"} variant={"contained"}>{sec ? `${sec}s` : "Get" }</Button>,classes: {input: 'big'}}}

                />

            </CardContent>

            <CardActions >
                <Button fullWidth disabled={!code || !codeSent} size={"large"} onClick={handleSubmit} variant="contained" color="primary">
                    Login
                </Button>
            </CardActions>
        </Card>
    </div>);
};

export default LoginPage;
