import {createContext, useContext, useEffect, useState} from 'react';
import axios from "../myaxios"
import {Navigate, Outlet, useLocation} from "react-router-dom";
import CurrentOrderDialog from "../app/orders/CurrentOderDialog";
import {useLastOrder} from "../app/orders/useLastOrder";

let AuthContext = createContext(null);

export function useAuth() {
    return useContext(AuthContext);
}

export const roles = {
    operator: "OPERATOR",
    driver: "DRIVER",
    guest: "GUEST"
};

const AuthProvider = ({children}) => {
    let [user, setUser] = useState(null);
    const [loading, setLoading] = useState(true);

    useEffect(()=>{
        checkToken().then(r => {
            setLoading(false)
        })
    },[]);

    const checkToken = async () => {
        // const token = localStorage.getItem("token");
        // if(!!token) {
        //     await signIn({phone: 1555285764, name: "Behrouz"});
        // }

        axios.get(`/checkToken`, ).then(res => {
            const sessionValid = res.data.result.session;
            // console.log("index.js | session is: " + sessionValid);
            switch (sessionValid) {
                case "VALID":
                    setUser(res.data.result.user);
                    break;
                case "INVALID":
                    signOut();
                    break;
                default:
                    console.log(sessionValid);
            }}).catch(er => {
                console.log(er);
            })
    };

    let signIn = ({phone, code}) => {
        return axios.post("/code", {phone, code}).then(res=> {
            console.log(res.data);
            setUser(res.data?.result?.user);
            localStorage.setItem("token", res.data?.result?.token);
        })
    };

    let signOut = () => {
        setUser(null);
        localStorage.removeItem("token");
        // axios.get("/logout").then(res=> {
        // })
    };

    const role = user ? user.role : "GUEST";

    let value = { user, role, signIn, signOut, checkToken, loading };

    return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
};


export function RequireAuth({ children }) {
    let auth = useAuth();
    let location = useLocation();
    const [current, setCurrent] = useState(false);

    const {order} = useLastOrder();

    useEffect(()=>{
        if(!!order) {
            setCurrent(true);
        }
    },[]);


    if (!auth.user && !auth.loading) {
        return <Navigate to="/login" state={{ from: location }} replace />;
    }

    if(!!children) return children;

    return <>
        {/*{current && <CurrentOrderDialog closeDialog={()=>setCurrent(false)} />}*/}
            <Outlet />
        </>;
}

export function RequireOperator({ children }) {
    let auth = useAuth();
    let location = useLocation();
    if(auth.role !== roles.operator) return <Navigate to="/login" state={{ from: location }} replace />;

    if(!!children) return children;
    return <>
        <Outlet />
    </>;
}


export default AuthProvider;
