// import Timer from "./Timer/Timer"
// import Checkout from "./Checkout";
import {
    Button, Card, CardContent, List, ListItem, ListItemSecondaryAction, ListItemText, Typography,
} from '@mui/material';
import {makeStyles} from "@mui/styles";
import {useState} from "react";
import {Directions} from "@mui/icons-material";
import clsx from "clsx";
import moment from "moment"
import {secToDur} from "../../profile/History";
import Loading from "../../../sharedComponents/Loading";
import Error500Page from "../../../sharedComponents/Error500Page";
import axios from "../../../myaxios";
import {useNavigate} from "react-router-dom";
import useSWR from "swr";
import {useMessage} from "../../../context/messageContext/MessageProvider";

//
const styles = makeStyles(theme => ({
    colorDisabled: {
        // color: Gray["600"],
        fontWeight: 'lighter',
        display: 'inline-block',
        verticalAlign: 'middle',
        marginRight: 7
    },
    wrapper: {
        backgroundColor: theme.palette.primary.main
    },
    circle: {
        width: "55vw",
        height: "55vw",
        marginBottom: 20,
        background: 'white',
        border: "10px solid rgba(255,255,255,0.5)",
        borderRadius: "50%",
        backgroundClip: 'padding-box',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
    }
}));

const StartTime  = ({order, onClose}) => {
    const classes = styles();
    const [loading, setLoading] = useState(false);
    const {showMessage} = useMessage();

    const navigate = useNavigate();

    const endEvent = () => {
        setLoading(true);
        axios.put(`/operator/event/end`, {orderId: order.id}).then(()=>{
            showMessage("Successfully ended", "success");
            onClose();
        }).finally(()=>setLoading(false))
    };

    if(loading) return <Loading />;
    return (
        <>
            <Card  square>
                <CardContent className={clsx("flex justify-center py-12 text-center", classes.wrapper)}>
                    <div className={classes.circle}>
                        {/*<Typography color={"textSecondary"}>Your park time</Typography>*/}
                        <Typography color={"textSecondary"} variant={"h6"}>Start: {moment(order.startDate).format("h:mma")}</Typography>
                        <Typography variant={"h3"} className={"font-bold"}>
                            {secToDur(order.duration)}
                        </Typography>
                    </div>
                </CardContent>
                <List >
                    <ListItem>
                        <ListItemText primary={"Parking"}/>
                        <ListItemSecondaryAction>
                           {order.parkingName}
                        </ListItemSecondaryAction>
                    </ListItem>
                    <ListItem>
                        <ListItemText primary={"Address"}/>
                        <ListItemSecondaryAction>
                            <Button
                                color={"primary"}
                                >
                                {order.address}
                            </Button>

                        </ListItemSecondaryAction>
                    </ListItem>
                    <ListItem>
                        <ListItemText primary={"Price"}/>
                        <ListItemSecondaryAction>
                            <Typography color="primary" variant={"h6"}>
                                {order.price}<small className={"text-gray-400"}>RMB</small>
                            </Typography>
                        </ListItemSecondaryAction>
                    </ListItem>
                </List>
            </Card>
            <div className="text-center p-5"><Button
                    onClick={endEvent}
                    color={"error"} size={"large"} fullWidth variant={"contained"}>STOP</Button>
            </div>
        </>
    );
};

export default StartTime;


