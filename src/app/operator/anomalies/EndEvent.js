import {useState} from 'react';
import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle, List, ListItem, ListItemSecondaryAction, ListItemText,
    TextField
} from "@mui/material";
import {Link, useNavigate} from "react-router-dom";
import axios from "../../../myaxios"
import {Close, Delete, Edit, Search} from "@mui/icons-material";
import IconButton from "@mui/material/IconButton";
import Chip from "@mui/material/Chip";
import {useMessage} from "../../../context/messageContext/MessageProvider";

const EndEvent = ({onClose, spot}) => {
    const [loading, setLoading] = useState(false);
    const navigate = useNavigate();
    const {showMessage} = useMessage();

    const onSubmit = e => {
        e.preventDefault();
        setLoading(true);
        axios.put(`/operator/event/end`, {spotId: spot.id}).then(()=>{
            onClose();
            showMessage("Successfully ended", 'success')
        }).finally(()=>setLoading(false))
    };

    return (<>
        <Dialog
            open={true}
            onClose={onClose}
            fullWidth
        >
            <div className={loading ? "loading" : ""}>
                <DialogTitle id="form-dialog-title">End orders on the spot</DialogTitle>
                <DialogContent>
                    <List dense>
                        <ListItem key={spot.id} className={"border-solid border-gray-200 rounded bg-gray-100 border-2"}>
                            <ListItemText
                                primary={spot.name}
                                secondary={spot.loraSerial}
                            />
                            <ListItemSecondaryAction className={" text-right"}>
                                <Chip size={"small"} label={spot.status ? "EMPTY" : "FULL"} color={spot.status ? "secondary" : "default"}/>
                            </ListItemSecondaryAction>
                        </ListItem>
                    </List>
                </DialogContent>
                <DialogActions>
                    <Button color={"inherit"} onClick={onClose}>Cancel</Button>
                    <Button disabled={!spot} onClick={onSubmit} variant={"contained"} color="error">
                        End event
                    </Button>
                </DialogActions>
            </div>
        </Dialog>
    </>);
};

export default EndEvent;
