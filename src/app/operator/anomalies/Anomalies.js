import {
    Button, Dialog, DialogActions, DialogContent, DialogContentText,
    DialogTitle,
    List,
    ListItem,
    ListItemSecondaryAction,
    ListItemText, TextField,
    Typography,
} from '@mui/material';
import useSWR from "swr";
import Error500Page from "../../../sharedComponents/Error500Page";
import Empty from "../../../sharedComponents/Empty";
import Loading from "../../../sharedComponents/Loading";
import Card from "@mui/material/Card";
import {useParams} from "react-router-dom";
import Chip from "@mui/material/Chip";
import {Delete, PlayArrow, Stop} from "@mui/icons-material";
import IconButton from "@mui/material/IconButton";
import {useState} from "react";
import axios from "../../../myaxios"
import {useMessage} from "../../../context/messageContext/MessageProvider";
import StartEvent from "./StartEvent";
import EndEvent from "./EndEvent";
import StartTime from "./StartTime";

export const secToDur = (seconds) => {
    let date = new Date(0);
    date.setSeconds(seconds);
    return  date.toISOString().substr(11, 8);
};

const Anomalies  = () => {
    const {showMessage} = useMessage();
    const {data, error, mutate} = useSWR(`/operator/anomalies`);
    const [loading, setLoading] = useState(false);

    const [startModal, setStartModal] = useState(false);
    const [stopModal, setStopModal] = useState(false);
    const [spot, setSpot] = useState(null);

    const [order, setOrder] = useState(null);

    const onSubmit = (e)=> {
        e.preventDefault();
        setLoading(true);
        // axios.put(`/operator/spots/${openDeleteDialog?.id}/status`, {status: "DISABLE"}).finally(()=>{
        //     setLoading(false);
        //     mutate()
        // }).then(()=>{
        //     showMessage("successfully deleted", "success")
        // })
    };

    const onClose=()=>{
        setOrder(null);
        setStartModal(false);
        setStopModal(false);
        mutate();
    };

    if (!data && !error) return <Loading />;
    if (error) return <Error500Page />;
    return (<>
            {startModal && <StartEvent setOrder={setOrder} onClose={onClose} spot={spot}/>}
            {stopModal && <EndEvent onClose={onClose}  spot={spot}/>}
        {data?.result?.length === 0 ? <Empty/>
            :
            <Card square>
                <List dense >
                    {data.result.map((_, i) =>
                        <ListItem key={i}>
                            <ListItemText
                                primary={_.spot.name}
                                secondary={_.lot.name}
                            />
                            <ListItemSecondaryAction className={" text-right"}>
                                <Chip size={"small"} label={_.type} />
                                <IconButton onClick={()=>{setSpot(_.spot); setStartModal(true)}} className={"ml-2"} color={"success"}><PlayArrow /></IconButton>
                                <IconButton onClick={()=>{setSpot(_.spot); setStopModal(true)}} className={"ml-2"} color={"error"}><Stop /></IconButton>
                            </ListItemSecondaryAction>
                        </ListItem>
                    )}
                </List>
            </Card>
        }


        <Dialog
            open={Boolean(order)}
            onClose={onClose}
            fullWidth
        >
            {order && <StartTime order={order} onClose={onClose} />}
        </Dialog>
    </>
    );
};

export default Anomalies;
