import {useState} from 'react';
import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle, List, ListItem, ListItemSecondaryAction, ListItemText,
    TextField
} from "@mui/material";
import {Link, useNavigate} from "react-router-dom";
import axios from "../../../myaxios"
import {Check, Close, Delete, Edit, Search} from "@mui/icons-material";
import IconButton from "@mui/material/IconButton";
import Chip from "@mui/material/Chip";
import Empty from "../../../sharedComponents/Empty";

const StartEvent = ({onClose, spot, setOrder}) => {

    // const [spot, setSpot]                = useState(null);
    const [plate, setPlate]              = useState(null);
    // const [searchSpot, setSearchSpot]    = useState('');
    const [searchPlate, setSearchPlate]  = useState('');
    const [plates, setPlates]            = useState(null);
    // const [spots, setSpots]              = useState(null);

    const [loading, setLoading] = useState(false);
    const navigate = useNavigate();

    const onSubmit = e => {
        e.preventDefault();
        setLoading(true);
        axios.post(`/operator/event/start`, {spotId: spot.id, plateId: plate.id}).then((res)=>{
            setOrder(res.data)
        }).finally(()=>setLoading(false))
    };

    // const onSearchSpot = e => {
    //     setLoading(true);
    //     axios.get(`/operator/searchSpot`, {params: {searchSpot}}).then(res=>{
    //         setSpots(res.data.result)
    //     }).finally(()=>setLoading(false))
    // };

    const onSearchPlate = e => {
        setLoading(true);
        axios.get(`/operator/searchPlate`, {params: {searchPlate}}).then(res=>{
            setPlates(res.data.result)
        }).finally(()=>setLoading(false))
    };

    return (<>
        <Dialog
            open={true}
            onClose={onClose}
            fullWidth
        >
            <div className={loading ? "loading" : ""}>
                <DialogTitle id="form-dialog-title">Start an event</DialogTitle>
                <DialogContent>
                    <List dense >
                        <ListItem key={spot.id} className={"border-solid border-gray-200 rounded bg-gray-100 border-2"}>
                            <ListItemText
                                primary={spot.name}
                                secondary={spot.loraSerial}
                            />
                            <ListItemSecondaryAction className={" text-right"}>
                                <Chip size={"small"} label={spot.status ? "EMPTY" : "FULL"} color={spot.status ? "secondary" : "default"}/>
                                {/*<IconButton onClick={() => setSpot(null)} className={"ml-2"} color={"error"}><Close /></IconButton>*/}
                            </ListItemSecondaryAction>
                        </ListItem>
                    </List>

                    {/*    <TextField*/}
                    {/*        autoFocus*/}
                    {/*        required*/}
                    {/*        margin="normal"*/}
                    {/*        id="spot"*/}
                    {/*        label="Search spot"*/}
                    {/*        fullWidth*/}
                    {/*        InputProps={{*/}
                    {/*            endAdornment: <IconButton onClick={onSearchSpot}><Search/></IconButton>,*/}
                    {/*        }}*/}
                    {/*        inputLabelProps={{shrink: true}}*/}
                    {/*        value={searchSpot}*/}
                    {/*        onChange={(e) => setSearchSpot(e.target.value)}*/}
                    {/*    />*/}
                    {/*{(spots  && searchSpot !== "" ) && <List disablePadding dense className={"bg-gray-100"}>*/}
                    {/*    {spots.length > 0 ? spots.map( _ =>*/}
                    {/*    <ListItem  button  onClick={() => setSpot(_)}  key={_.id}>*/}
                    {/*        <ListItemText*/}
                    {/*            primary={_.name}*/}
                    {/*            secondary={_.loraSerial}*/}
                    {/*        />*/}
                    {/*        <ListItemSecondaryAction className={" text-right"}>*/}
                    {/*            <Chip size={"small"} label={_.status ? "EMPTY" : "FULL"} color={_.status ? "secondary" : "default"}/>*/}
                    {/*        </ListItemSecondaryAction>*/}
                    {/*    </ListItem>)*/}
                    {/*        : <Empty/>}*/}
                    {/*</List>*/}
                    {/*}*/}

                    {/*</>*/}
                    {/*}*/}
                    {plate ? <List dense className={"border-solid border-gray-200 rounded bg-gray-100 border-2"}>
                        <ListItem key={plate.id}>
                            <div className={"rounded h-5 w-5 mr-4"} style={{backgroundColor: plate.color}}/>
                            <ListItemText
                                primary={plate.plateNumber}
                                secondary={plate.brand}
                            />
                            <ListItemSecondaryAction>
                                <IconButton onClick={() => setPlate(null)} className={"ml-2"} color={"error"}><Close /></IconButton>
                            </ListItemSecondaryAction>
                        </ListItem>
                    </List> : <><TextField
                        margin="normal"
                        id="plate"
                        required
                        label="Search plate number"
                        fullWidth
                        inputLabelProps={{shrink: true}}
                        value={searchPlate}
                        InputProps={{
                            endAdornment: <IconButton onClick={onSearchPlate}><Search /></IconButton>,
                        }}
                        onChange={(e)=>setSearchPlate( e.target.value)}
                    />
                        {(plates  && searchPlate !== "" ) && <List disablePadding dense className={"bg-gray-100"}>
                            {plates.length > 0 ? plates.map( _ =>
                                    <ListItem  button  onClick={() => setPlate(_)}  key={_.id}>
                                        <div className={"rounded h-5 w-5 mr-4"} style={{backgroundColor: _.color}}/>
                                        <ListItemText
                                            primary={_.plateNumber}
                                            secondary={_.brand}
                                        />
                                    </ListItem>)
                                : <Empty/>}
                        </List>
                        }
                    </>}
                </DialogContent>
                <DialogActions>
                    <Button color={"inherit"} onClick={onClose}>Cancel</Button>
                    <Button disabled={!plate || !spot} onClick={onSubmit} variant={"contained"} color="primary">
                        Start event
                    </Button>
                </DialogActions>
            </div>
        </Dialog>
        </>);
};

export default StartEvent;
