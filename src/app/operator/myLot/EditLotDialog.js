import axios from "../../../myaxios"

import {Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, TextField,} from '@mui/material';
import {useState} from "react";
import {useAuth} from "../../../auth/AuthProvider";
import {useMessage} from "../../../context/messageContext/MessageProvider";

const EditLotDialog  = ({closeDialog , lot}) => {
    const m = useMessage();
    const [name, setName] = useState(lot.name);
    const [address, setAddress] = useState(lot.address.fullAddress);

    const [loading, setLoading] = useState(false);

    const onSubmit = (e) => {
        e.preventDefault();
        setLoading(true);
        axios.put("/operator/lots", {id: lot.id , name, address}).then(()=>{
            setLoading(false);
            closeDialog();
            m.showMessage("Lot updated", "success")
        }).finally(()=>setLoading(false))
    };

    return (
        <Dialog
            open={true}
            onClose={closeDialog}
            fullWidth
            >
            <form onSubmit={onSubmit}  className={loading ? "loading" : ""}>
                <DialogTitle id="form-dialog-title">Edit</DialogTitle>
                <DialogContent>
                    <TextField
                        autoFocus
                        margin="normal"
                        id="name"
                        label="Name"
                        fullWidth
                        value={name}
                        onChange={(e)=>setName(e.target.value)}
                    />
                    <TextField
                        autoFocus
                        margin="normal"
                        id="Address"
                        label="Address"
                        fullWidth
                        value={address}
                        onChange={(e)=>setAddress(e.target.value)}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={closeDialog}>Cancel</Button>
                    <Button type={"submit"} variant={"contained"} color="primary">
                        Save
                    </Button>
                </DialogActions>
            </form>
        </Dialog>
    );
};

export default EditLotDialog;
