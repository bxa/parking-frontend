import {
    Button, Dialog, DialogActions, DialogContent, DialogContentText,
    DialogTitle,
    List,
    ListItem,
    ListItemSecondaryAction,
    ListItemText, TextField,
    Typography,
} from '@mui/material';
import useSWR from "swr";
import Error500Page from "../../../sharedComponents/Error500Page";
import Empty from "../../../sharedComponents/Empty";
import Loading from "../../../sharedComponents/Loading";
import Card from "@mui/material/Card";
import {useParams} from "react-router-dom";
import Chip from "@mui/material/Chip";
import {Delete} from "@mui/icons-material";
import IconButton from "@mui/material/IconButton";
import {useState} from "react";
import axios from "../../../myaxios"
import {useMessage} from "../../../context/messageContext/MessageProvider";

export const secToDur = (seconds) => {
    let date = new Date(0);
    date.setSeconds(seconds);
    return  date.toISOString().substr(11, 8);
};

const Spots  = () => {
    const {id} = useParams();
    const {showMessage} = useMessage();
    const {data, error, mutate} = useSWR(id ? `/operator/lots/${id}/spots` : null);
    const [loading, setLoading] = useState(false);
    const [openDeleteDialog, setOpenDeleteDialog] = useState(null);

    const onSubmit = (e)=> {
        e.preventDefault();
        setLoading(true);
        axios.put(`/operator/spots/${openDeleteDialog?.id}/status`, {status: "DISABLE"}).finally(()=>{
            setOpenDeleteDialog(null);
            setLoading(false);
            mutate()
        }).then(()=>{
            showMessage("successfully deleted", "success")
        })
    };

    if (!data && !error) return <Loading />;
    if (error) return <Error500Page />;
    return (<>
        {data?.result?.length === 0 ? <Empty/>
            :
            <Card square>
                <List dense >
                    {data.result.map((_, i) =>
                        <ListItem key={_.id}>
                            <ListItemText
                                primary={_.name}
                                secondary={_.loraSerial}
                            />
                            <ListItemSecondaryAction className={" text-right"}>
                                <Chip size={"small"} label={_.status ? "EMPTY" : "FULL"} color={_.status ? "secondary" : "default"}/>
                                <IconButton onClick={()=>setOpenDeleteDialog(_)} className={"ml-2"} color={"error"}><Delete /></IconButton>
                            </ListItemSecondaryAction>
                        </ListItem>
                    )}
                </List>
            </Card>
        }


            <Dialog
                open={Boolean(openDeleteDialog)}
                onClose={()=>setOpenDeleteDialog(null)}
                fullWidth
            >
                <form onSubmit={onSubmit}  className={loading ? "loading" : ""}>
                    <DialogTitle id="form-dialog-title">Name</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Are you sure you want to delete {openDeleteDialog?.name}
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={()=>setOpenDeleteDialog(null)}>Cancel</Button>
                        <Button type={"submit"} variant={"contained"} color="error">
                            Delete
                        </Button>
                    </DialogActions>
                </form>
            </Dialog>
    </>
    );
};

export default Spots;
