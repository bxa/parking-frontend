import profilePic from '../../../img/uxceo-128.jpg';
import {Link} from "react-router-dom";
import {
    Avatar, Button, Card, CardContent, Divider, Grid, List, ListItem, ListItemSecondaryAction, ListItemText, Typography,
} from '@mui/material';
import {useAuth} from "../../../auth/AuthProvider";
import ListItemIcon from "@mui/material/ListItemIcon";
import {makeStyles} from "@mui/styles";
import {
    AirportShuttle,
    Assignment,
    ChevronRight,
    Edit,
    ExpandMore, Room,
} from "@mui/icons-material";
import {useEffect, useState} from "react";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import useSWR from "swr";
import Loading from "../../../sharedComponents/Loading";
import Error500Page from "../../../sharedComponents/Error500Page";
import NameDialog from "../../profile/settings/NameDialog";
import EditLotDialog from "./EditLotDialog";

const styles = makeStyles(theme => ({
    card: {
        background: theme.palette.primary.dark
    },
}));

const MyLot  = () => {
    const {user, signOut} = useAuth();

    const { data, error, mutate} = useSWR(`/operator/lots`);
    const [masterLot, setMasterLot] = useState();
    const [loading, setLoading] = useState(false);
    const [openEdit, setOpenEdit] = useState(false);

    useEffect(()=>{
        const m = localStorage.getItem("masterLot");
        // console.log('upadats', m, data );
        if(m && !!data) {
            const i = data.result.findIndex(e => e.id === m);
            // console.log("it has localstorage and data",i);
            if(i > -1)
                setMasterLot( data.result[i]);
            else
                setMasterLot( data.result[0])
        }
        else if(!!data && data?.result.length > 0) {
            // console.log("it doesn't have localstorage but it has data");
            localStorage.setItem("masterLot", data.result[0]);
            setMasterLot(data.result[0].id)
        }
    },[data]);

    const classes = styles();

    const [anchorEl, setAnchorEl] = useState(null);
    const open = Boolean(anchorEl);
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };
    const handleClickMenu = (lot) => (e) => {
        setMasterLot(lot);
        handleClose();
        localStorage.setItem("masterLot", lot.id);
    };

    if (!data && !error || loading ) return <Loading />;
    if (error) return <Error500Page />;


    return (
        <>
            {openEdit && <EditLotDialog  closeDialog={()=>setOpenEdit(false)} lot={masterLot}/>}
            <Card
                square
                className={classes.card}
            >
                <CardContent>
                    <div className={'flex items-center justify-between'}>
                        {data.result.length > 0 ? <>
                            <button
                                className={"bg-transparent border-0 flex items-center text-left"}
                                id="basic-button"
                                aria-controls={open ? 'basic-menu' : undefined}
                                aria-haspopup="true"
                                aria-expanded={open ? 'true' : undefined}
                                onClick={handleClick}
                            >
                                <div>
                                    <Typography variant={"h6"} className={"capitalize text-white"}>{masterLot?.name}</Typography>
                                    <Typography variant={"caption"} className={"opacity-75 capitalize text-white"}>{masterLot?.address.fullAddress}</Typography>
                                </div>
                                <ExpandMore className={"text-white ml-2"} fontSize={"large"}/>
                            </button>
                            <Menu
                                id="basic-menu"
                                anchorEl={anchorEl}
                                open={open}
                                onClose={handleClose}
                                MenuListProps={{
                                    'aria-labelledby': 'basic-button',
                                }}

                                transformOrigin={{
                                    vertical: 'top', horizontal: 'left',
                                }}
                            >
                                {data.result.map(_ => <MenuItem key={_.id} onClick={handleClickMenu(_)} className={"block"}>
                                    <Typography>{_.name}</Typography>
                                    <Typography variant={"caption"} className={"opacity-75 "}>{_.address.fullAddress}</Typography>
                                </MenuItem>)}
                            </Menu>
                        </> :
                            <div>
                                <Typography variant={"h6"} className={"capitalize text-white"}>{masterLot?.name}</Typography>
                                <Typography variant={"caption"} className={"opacity-75 capitalize text-white"}>{masterLot?.address.fullAddress}</Typography>
                            </div>
                        }

                        <Button onClick={()=>setOpenEdit(true)} startIcon={<Edit />} variant={"contained"} >Edit</Button>
                    </div>
                </CardContent>
            </Card>
            <Card square>
                <CardContent>
                <div className={"flex text-center"}>
                    <div className={"flex-grow"}>
                        <Typography variant={"h5"} color={"primary"}>{masterLot?.emptySpace}</Typography>
                        <Typography variant={"caption"}>Empty spaces</Typography>
                    </div>
                    <div className={"flex-grow"}>
                        <Typography variant={"h5"} >{masterLot?.capacity}</Typography>
                        <Typography variant={"caption"}>Spaces</Typography>
                    </div>
                    <div className={"flex-grow"}>
                        <Typography variant={"h5"} color={"error"} >{masterLot?.anomalies}</Typography>
                        <Typography variant={"caption"}>Anomalies</Typography>
                    </div>
                </div>
                </CardContent>
                <Divider light/>
                <List disablePadding>
                    <ListItem
                        component={Link}  to={`/operator/lots/${masterLot?.id}/spots`} button >
                        <ListItemIcon><Room /></ListItemIcon>
                        <ListItemText >Spots</ListItemText>
                        <ListItemSecondaryAction>
                            <ChevronRight className={"mt-2"} />
                        </ListItemSecondaryAction>
                    </ListItem>
                    <Divider light component={'li'}/>
                    <ListItem
                        component={Link} button  to={`/operator/lots/${masterLot?.id}/events`}>
                        <ListItemIcon><Assignment /></ListItemIcon>
                        <ListItemText >History of Events</ListItemText>
                        <ListItemSecondaryAction>
                            <ChevronRight className={"mt-2"} />
                        </ListItemSecondaryAction>
                    </ListItem>
                    <Divider light component={'li'}/>
                    <ListItem  component={Link}  to={`/operator/lots/${masterLot?.id}/transactions`} button >
                        <ListItemIcon><AirportShuttle /></ListItemIcon>
                        <ListItemText >Transactions</ListItemText>
                        <ListItemSecondaryAction>
                            <ChevronRight className={"mt-2"} />
                        </ListItemSecondaryAction>
                    </ListItem>
                </List>
            </Card>
        </>
    );

};

export default MyLot;
