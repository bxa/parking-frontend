import {List, ListItem, ListItemSecondaryAction, ListItemText, Typography,} from '@mui/material';
import useSWR from "swr";
import Card from "@mui/material/Card";
import {useParams} from "react-router-dom";
import Chip from "@mui/material/Chip";
import Loading from "../../sharedComponents/Loading";
import Error500Page from "../../sharedComponents/Error500Page";
import Empty from "../../sharedComponents/Empty";
import moment from "moment";

export const secToDur = (seconds) => {
    let date = new Date(0);
    date.setSeconds(seconds);
    return  date.toISOString().substr(11, 8);
};

const Events  = () => {
    const {id} = useParams();
    const {data, error} = useSWR(id ? `/operator/lots/${id}/events` : null);

    if (!data && !error) return <Loading />;
    if (error) return <Error500Page />;
    return (<>
        {data?.result?.length === 0 ? <Empty/>
            :
            <Card square>
                <List dense>
                    {data.result.map((_, i) =>
                        <ListItem key={i}>
                            <ListItemText
                                primary={_.spot.name}
                                secondary={moment(_.createdAt).fromNow()}
                            />
                            <ListItemSecondaryAction className={"pr-3 text-right"}>
                                <Chip size={"small"} label={_.type} color={_.type === "PARK" ? "secondary" : "default"}/>
                            </ListItemSecondaryAction>
                        </ListItem>
                    )}
                </List>
            </Card>
        }
    </>
    );
};

export default Events;
