import axios from "../../../myaxios"

import {Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, TextField,} from '@mui/material';
import {useState} from "react";
import {useAuth} from "../../../auth/AuthProvider";
import {useMessage} from "../../../context/messageContext/MessageProvider";

const NameDialog  = ({closeDialog}) => {
    const {user} = useAuth();
    const m = useMessage();

    const [loading, setLoading] = useState(false);
    const [value, setValue] = useState(user ? user.name : "");

    const onSubmit = (e) => {
        e.preventDefault();
        setLoading(true);
        axios.put("/profile", {name: value}).then(()=>{
            setLoading(false);
            closeDialog("changeName")();
            m.showMessage("Profile updated", "success")
        }).finally(()=>setLoading(false))
    };

    return (
        <Dialog
            open={true}
            onClose={closeDialog("changeName")}
            fullWidth
            >
            <form onSubmit={onSubmit}  className={loading ? "loading" : ""}>
                <DialogTitle id="form-dialog-title">Name</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Please enter your name
                    </DialogContentText>
                    <TextField
                        autoFocus
                        margin="normal"
                        id="name"
                        label="Name"
                        fullWidth
                        value={value}
                        onChange={(e)=>setValue(e.target.value)}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={closeDialog('changeName')}>Cancel</Button>
                    <Button type={"submit"} variant={"contained"} color="primary">
                        Save
                    </Button>
                </DialogActions>
            </form>
        </Dialog>
    );
};

export default NameDialog;
