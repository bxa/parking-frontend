import {Link} from "react-router-dom";
import axios from "../../../myaxios"
import {Divider, IconButton, List, ListItem, ListItemSecondaryAction, ListItemText, Typography,} from '@mui/material';
import {useState} from "react";
import ListItemIcon from "@mui/material/ListItemIcon";
import {Info, NotificationsActive, NotificationsOff} from "@mui/icons-material";

const Settings  = (props) => {
    const [notification, setNotification] = useState(true);
    // const [version, setVersion] = useState("0.1.1");
    const [loading, setLoading] = useState(false);
    // const = () => {
    //     axios.get('/settings').then(res => {
    //         setState({settings: res.data, notification:  res.data.notification})
    //     })
    // };

    const toggleNotification = () => {
        setLoading(true);
        axios.put('/settings', {notification: !notification}).then(res => {
            setNotification(res.data.notification);
            setLoading(false)
        }).catch(()=>{
            setLoading(false)
        });
    };

    // function checkForUpdate() {
    //     setLoading(false);
    //     axios.put('/checkUpdate', {notification: !state.notification}).then(res => {
    //         setNotification(res.data.notification);
    //         setVersion(res.data.version);
    //         setLoading(false)
    //     });
    // }

    return <>
        <List disablePadding>
            <ListItem className={loading ? "loading" : ""}>
                <ListItemText >Notifications</ListItemText>
                <ListItemSecondaryAction>
                    <IconButton onClick={toggleNotification} size="large">
                    {notification ?
                        <NotificationsActive />: <NotificationsOff color={"primary"}/>
                    }
                    </IconButton>
                </ListItemSecondaryAction>
            </ListItem>
            <Divider light component={'li'}/>
            {/*<ListItem onClick={checkForUpdate} button >*/}
            {/*    <Icon>system_update</Icon>*/}
            {/*    <ListItemText >Check for Update</ListItemText>*/}
            {/*    <ListItemSecondaryAction>*/}
            {/*        {!!version ? version : <Icon>chevron_right</Icon>}*/}
            {/*    </ListItemSecondaryAction>*/}
            {/*</ListItem>*/}
            {/*<Divider light component={'li'}/>*/}
        </List>
        <List>
            <ListItem component={Link} to={"/about"} button >
            <ListItemIcon><Info/></ListItemIcon>
                <ListItemText>About Us</ListItemText>
                <ListItemSecondaryAction >
                    <Typography variant={"caption"}>01/25/2018</Typography>
                </ListItemSecondaryAction>
            </ListItem>
        </List>
    </>
};

export default Settings;
