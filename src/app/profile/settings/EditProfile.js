import profilePic from '../../../img/uxceo-128.jpg';
import NameDialog from "./NameDialog";
import GenderDialog from "./GenderDialog";
import {
    Avatar, Card, CardContent, Divider, List, ListItem, ListItemSecondaryAction, ListItemText,
} from '@mui/material';
import {makeStyles} from "@mui/styles";
import {useState} from "react";
import Button from "@mui/material/Button";
import {ChevronRight} from "@mui/icons-material";

const styles = makeStyles(theme => ({
    bigAvatar: {
        width: 90,
        height: 90,
        border: '5px solid rgba(255,255,255,0.4)',
        margin: 'auto'
    },
    card: {
        background: theme.palette.primary.dark
    },
}));

const EditProfile  = () => {
    const classes = styles();
    const [modals, setModals] = useState({});

    const openDialog = (name) =>  () => {
        setModals(modals => ({...modals, [name]: true}))
    };

    const closeDialog = (name) => () => {
        setModals(modals => ({...modals, [name]: false}))
    };

    return (
        <>
            <Card square>
                <CardContent className={"text-center"}>
                    <Avatar
                        alt="Adelle Charles"
                        src={profilePic}
                        className={classes.bigAvatar}
                    />
                    <Button color={"textSecondary"} variant={"subtitle1"} className={"mt-4 capitalize"}>
                        Upload Profile Picture
                    </Button>
                </CardContent>
                <Divider light />
                <List disablePadding>
                    <ListItem onClick={openDialog('changeName')} button>
                        <ListItemText>Name</ListItemText>
                        <ListItemSecondaryAction>
                            <ChevronRight className={'mt-1'} />
                        </ListItemSecondaryAction>
                    </ListItem>
                    <Divider light component={'li'}/>
                    <ListItem onClick={openDialog('changeGender')} button to="/user/payments">
                        <ListItemText >Gender</ListItemText>
                        <ListItemSecondaryAction>
                            <ChevronRight className={'mt-1'} />
                        </ListItemSecondaryAction>
                    </ListItem>
                </List>
            </Card>

        {modals['changeName'] && <NameDialog closeDialog={closeDialog}/> }
        {/*{modals['changeBirthday'] && <BirthdayDialog closeDialog={closeDialog}/> }*/}
        {modals['changeGender'] && <GenderDialog closeDialog={closeDialog}/> }
            </>
    );

};

export default EditProfile;

