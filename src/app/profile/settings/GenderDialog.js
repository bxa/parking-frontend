import axios from "../../../myaxios"

import {
    Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, MenuItem, TextField,
} from '@mui/material';
import {useAuth} from "../../../auth/AuthProvider";
import {useState} from "react";
import {useMessage} from "../../../context/messageContext/MessageProvider";

const GenderDialog  = ({closeDialog}) => {
    const {user} = useAuth();
    const {showMessage} = useMessage();

    const [loading, setLoading] = useState(false);
    const [value, setValue] = useState(user ? user.gender ? user.gender  : "" : "");

    const onSubmit = (e) => {
        e.preventDefault();
        setLoading(true);
        axios.put("/profile", {gender: value}).then(()=>{
            closeDialog("changeGender")();
            showMessage("Profile updated", "success")
        }).finally(()=>setLoading(false))
    };


        return (
            <Dialog
                open={true}
                onClose={closeDialog("changeGender")}
                fullWidth
            >
                <form onSubmit={onSubmit}  className={loading ? "loading" : ""}>
                    <DialogTitle id="form-dialog-title">Gender</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Please enter your gender
                        </DialogContentText>
                        <TextField
                            select
                            autoFocus
                            margin="dense"
                            id="gender"
                            label="Gender"
                            fullWidth
                            value={value}
                            onChange={(e)=>setValue( e.target.value)}
                        >
                            <MenuItem value={"MALE"}>
                                Male
                            </MenuItem>
                            <MenuItem  value={"FEMALE"}>
                                Female
                            </MenuItem>
                            <MenuItem  value={"NONE"}>
                                None
                            </MenuItem>
                        </TextField>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={closeDialog('changeGender')}>Cancel</Button>
                        <Button type={"submit"} variant={"contained"}
                                color="primary">
                            Save
                        </Button>
                    </DialogActions>
                </form>
            </Dialog>
        );

};

export default GenderDialog;
