import {Card, Chip, List, ListItem, ListItemSecondaryAction, ListItemText, Typography,} from '@mui/material';
import useSWR from "swr";
import Empty from "../../sharedComponents/Empty";
import Error500Page from "../../sharedComponents/Error500Page";
import Loading from "../../sharedComponents/Loading";
import moment from "moment";
import {secToDur} from "./History";

const Payments  = () => {
    const {data, error} = useSWR('/profile/transactions');

    if (!data && !error) return <Loading />;
    if (error) return <Error500Page />;
    return <>
        {data.result?.length === 0 ? <Empty/>
            :
            <Card square>
                <List dense>
                    {data.result?.map((_i, i) =>
                        <ListItem key={i}>
                            <ListItemText
                                primary={<><Chip size={"small"} label={_i.type} color={"primary"}/> {secToDur(_i.order.duration)}</>}
                                secondary={_i.order.parkingName + " - " + moment(_i.date).fromNow()}
                            />
                            <ListItemSecondaryAction className={"pr-3 text-left"}>
                                <Typography variant={"h6"} className={"text-blue-400"}>{_i.amount}RMB</Typography>
                            </ListItemSecondaryAction>
                        </ListItem>
                    )}
                </List>
            </Card>
        }
    </>;
};

export default Payments;
