import profilePic from '../../img/uxceo-128.jpg';
import {Link} from "react-router-dom";
import {
    Avatar, Button, Card, CardContent, Divider, List, ListItem, ListItemSecondaryAction, ListItemText, Typography,
} from '@mui/material';
import {useAuth} from "../../auth/AuthProvider";
import ListItemIcon from "@mui/material/ListItemIcon";
import {makeStyles} from "@mui/styles";
import {AirportShuttle, Assessment, Assignment, ChevronRight, Edit, Settings} from "@mui/icons-material";
import Chip from "@mui/material/Chip";

const styles = makeStyles(theme => ({
    bigAvatar: {
        width: 90,
        height: 90,
        border: '5px solid rgba(255,255,255,0.4)',
        margin:"auto"
    },
    card: {
        background: theme.palette.primary.dark
    },
}));

const MyProfile  = () => {
    const {user, signOut} = useAuth();
    const classes = styles();

    return (
        <>
            <Card
                square
                className={classes.card}
            >
                <CardContent className={"pt-8"}>
                    <Avatar
                        alt="Adelle Charles"
                        src={profilePic}
                        className={classes.bigAvatar}
                    />
                    <div className={"text-center"}>
                        <Typography className={"text-white mt-3"} variant={'h5'}>{user?.name} <Chip size={"small"} label={user?.role} color={"info"}/></Typography>
                        <Button className={"text-lg text-white opacity-75"} component={Link} to="/profile/edit">
                            {user?.phone}
                            <Edit className={'ml-2 text-md'} />
                        </Button>
                    </div>
                </CardContent>
            </Card>
            <Card square>
                <List disablePadding>
                    <ListItem
                        component={Link}  to="/profile/history" button >
                        <ListItemIcon><Assessment /></ListItemIcon>
                        <ListItemText >History</ListItemText>
                        <ListItemSecondaryAction>
                            <ChevronRight className={"mt-2"} />
                        </ListItemSecondaryAction>
                    </ListItem>
                    <Divider light component={'li'}/>
                    <ListItem

                        component={Link} button  to="/profile/payments">
                        <ListItemIcon><Assignment /></ListItemIcon>
                        <ListItemText >Payment</ListItemText>
                        <ListItemSecondaryAction>
                            <ChevronRight className={"mt-2"} />
                        </ListItemSecondaryAction>
                    </ListItem>
                    <Divider light component={'li'}/>
                    <ListItem  component={Link}  to={'/profile/vehicles'} button >
                        <ListItemIcon><AirportShuttle /></ListItemIcon>
                        <ListItemText >Cars</ListItemText>
                        <ListItemSecondaryAction>
                            {user?.mainCar ? <Typography variant={"caption"} >{user?.mainCar?.plate}</Typography>
                            : <ChevronRight className={"mt-2"} />}
                        </ListItemSecondaryAction>
                    </ListItem>
                    <Divider light component={'li'}/>

                    <ListItem  component={Link} button   to="/profile/settings">
                        <ListItemIcon><Settings /></ListItemIcon>

                        <ListItemText >Settings</ListItemText>
                        <ListItemSecondaryAction >
                            <ChevronRight className={"mt-2"} />
                        </ListItemSecondaryAction>
                    </ListItem>
                    <Divider light component={'li'}/>
                </List>
            </Card>

            <List>
                <ListItem onClick={signOut} component={'li'} button >
                    <ListItemText className={"text-red-500 text-center"}>Logout</ListItemText>
                </ListItem>
            </List>

        </>
    );

};

export default MyProfile;
