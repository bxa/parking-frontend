import {List, ListItem, ListItemSecondaryAction, ListItemText, Typography,} from '@mui/material';
import useSWR from "swr";
import Error500Page from "../../sharedComponents/Error500Page";
import Empty from "../../sharedComponents/Empty";
import Loading from "../../sharedComponents/Loading";
import moment from "moment";

import AvTimerIcon from '@mui/icons-material/AvTimer';
import Card from "@mui/material/Card";

export const secToDur = (seconds) => {
    let date = new Date(0);
    date.setSeconds(seconds);
    return  date.toISOString().substr(11, 8);
};

const History  = () => {
    const {data, error} = useSWR('/profile/orders');

    if (!data && !error) return <Loading />;
    if (error) return <Error500Page />;
    return (<>
        {data?.result?.length === 0 ? <Empty/>
            :
            <Card square>
                <List dense>
                    {data.result.map((_i, i) =>
                        <ListItem key={_i.id}>
                            <ListItemText
                                primary={_i.parkingName}
                                secondary={_i.address}
                            />
                            <ListItemSecondaryAction className={"pr-3 text-right"}>
                                <Typography variant={"caption"}>{moment(_i.date).fromNow()}</Typography>
                                <Typography className={"flex items-center"}><AvTimerIcon fontSize={"small"} className={"text-gray-400 align-middle"}/> <span  className={"text-blue-400"}>{secToDur(_i.duration)}</span></Typography>
                            </ListItemSecondaryAction>
                        </ListItem>
                    )}
                </List>
            </Card>
        }
    </>
    );
};

export default History;
