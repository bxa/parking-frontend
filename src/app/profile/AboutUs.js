import {Card, CardContent, Divider, Typography} from '@mui/material';
import Header from "../../sharedComponents/Header";
import {makeStyles} from "@mui/styles";
import {CarRepair} from "@mui/icons-material";


export const styles = makeStyles(theme => ({
    lineHeight: {lineHeight: 2},
    logo: {
        color: theme.palette.primary.main,
        margin: "10px 0",
        fontSize: 130
    }
}));

const AboutUs  = () => {
    const classes = styles();
        return <>
            <Header title={"About us"} />

            <Card>
                <CardContent className={"leading-2"}>
                    <div className="text-center">
                        <CarRepair  className={classes.logo}/>
                        <Typography variant={"h4"}>Smart Parking System</Typography>
                        <Typography>base of <strong>LoRa</strong></Typography>
                    </div>
                    <Divider className={"my-3"}/>
                    <Typography variant={"body1"}>Finding a vacant parking space in a congested area or a large
                        parking lot, especially, in peak hours, is always time consuming and frustrating to drivers.
                        It is common for drivers to keep circling a parking lot and look for a vacant parking space.
                        Real-time street parking availability information is important in urban areas, and if
                        available could reduce congestion, pollution, and gas consumption. </Typography>
                    <Divider  className={"my-3"}/>
                    <Typography>From <strong>Southwest Jiaotong University</strong></Typography>
                    <div className={"flex justify-between mt-1"}><Typography>Behrooz Erfanian </Typography><Typography
                        color={"primary"}> +8615528073601</Typography></div>
                </CardContent>
            </Card>

            </>

};

export default AboutUs;
