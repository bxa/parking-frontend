import {useState} from 'react';

import axios from "myaxios";
import {
    Avatar, Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Icon, TextField, Typography
} from "@mui/material";
import profileNotLoggedIn from "../../img/profile.png";

const Login = () => {
    const [phone, setPhone] = useState("");
    const [loading, setLoading] = useState(false);
    const [step, setStep] = useState(0);

    const sendSMS = () => {
        setLoading(true);
        axios.post('/getCode', {mobile: state.phone}).then(() => {
            setLoading(false);
            setStep(1)
        })
    };

    return (<>
        <Avatar
            alt="Adelle Charles"
            src={profileNotLoggedIn}
            className={classNames(props.classes.avatar, props.classes.bigAvatar)}
        />
        <Typography  onClick={handleClickOpenLogin} variant={"h6"}
                     className={props.classes.label + " text-center"}>
            Login <Icon className={'vm'}>chevron_right</Icon>
        </Typography>
        {props.dialogs['login'] &&
        <Dialog
            open={props.dialogs['login']}
            onClose={() => props.closeDialog("login")}
            aria-labelledby="form-dialog-title"
            className={state.loading ? "loading" : ""}
        >
            <DialogTitle id="form-dialog-title">Login</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    Please enter your mobile number
                </DialogContentText>
                <TextField
                    autoFocus
                    margin="dense"
                    id="name"
                    label="Phone number"
                    type="number"
                    fullWidth
                    value={state.phone}
                    onChange={(e)=>setState({phone: e.target.value})}
                />
            </DialogContent>
            <DialogActions>
                <Button onClick={sendSMS} fullWidth size={"large"} color="primary">
                    Send SMS
                </Button>
            </DialogActions>
        </Dialog>
        }
        {props.dialogs['confirmCode'] &&
        <Dialog
            open={props.dialogs["confirmCode"]}
            onClose={() => props.closeDialog("confirmCode")}
            aria-labelledby="form-dialog-title"
        >
            <DialogContent>
                <DialogContentText>
                    Enter the code we've sent you
                </DialogContentText>
                <TextField
                    autoFocus
                    margin="dense"
                    id="code"
                    label="Confirmation code"
                    type="number"
                    fullWidth
                    value={state.code}
                    onChange={(e)=>setState({code: e.target.value})}
                    InputProps={{classes: {input: 'big'}}}
                    InputLabelProps={{
                        shrink: true,
                    }}
                />
            </DialogContent>
            <DialogActions>
                <Button onClick={() => props.closeDialog("confirmCode")} variant={"text"}>
                    <Icon>chevron_left</Icon> Change
                </Button>
                <Button onClick={() => {props.closeDialog("confirmCode"); props.closeDialog("login"); props.login(state.phone, state.code)}} variant={"text"}
                        color="primary">
                    Send
                </Button>
            </DialogActions>
        </Dialog>
        }
    </>)
};

export default Login;
