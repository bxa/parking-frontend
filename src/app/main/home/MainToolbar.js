import {AppBar, IconButton, Input, InputAdornment, Toolbar} from '@mui/material';
import {useState} from "react";
import {CarRepair, CropFree, Search} from "@mui/icons-material";
import {useAuth} from "../../../auth/AuthProvider";
import EnterId from "./EnterId";

// const styles = {
//     root: {
//         background: 'white',
//         borderRadius: 3,
//         paddingLeft: 10,
//         flexGrow: 1,
//     },
//     flex: {
//
//         flex: 1,
//     },
//     searchcolor: {
//         color: '#ccc'
//     },
//     gutters: {
//         paddingRight: 5
//     }
// };

const MainToolbar  = ({}) => {

    const {user} = useAuth();
    const [enterId, setEnterId] = useState(false);

    return (<>
        {enterId && <EnterId closeDialog={()=>setEnterId(false)}/>}

        <AppBar position="fixed">
            <Toolbar>
                <Input
                    placeholder="Search Here"
                    className={"flex"}
                    classes={{
                        root: "bg-white rounded pl-2 flex-grow",
                    }}
                    inputProps={{
                        'aria-label': 'Description',
                    }}
                    startAdornment={
                        <InputAdornment position="start">
                            <Search />
                        </InputAdornment>
                    }
                />
                <IconButton
                    color="inherit"
                    size="large">
                    <CropFree />
                </IconButton>

                <IconButton
                    onClick={()=>setEnterId(true)}
                    color="inherit"
                    size="large">
                    <CarRepair />
                </IconButton>
            </Toolbar>
        </AppBar>
        </>
    );
};

export default MainToolbar;
