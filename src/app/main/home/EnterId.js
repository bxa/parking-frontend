import {useState} from 'react';
import {Button, Dialog, DialogActions, DialogContent, DialogTitle, TextField} from "@mui/material";
import {useNavigate} from "react-router-dom";
import axios from "../../../myaxios"

const EnterId = ({closeDialog}) => {

    const [value, setValue] = useState('');
    const [loading, setLoading] = useState(false);
    const navigate = useNavigate();

    const onSubmit = e => {
        e.preventDefault();
        setLoading(true);
        axios.post(`/events/start`, {spotId: value}).then(()=>{
            navigate(`/start`)
        }).finally(()=>setLoading(false))
    };

    return (<>
        <Dialog
            open={true}
            onClose={closeDialog}
            fullWidth
        >
            <form onSubmit={onSubmit}  className={loading ? "loading" : ""}>
                <DialogTitle id="form-dialog-title">Please enter the spot Id</DialogTitle>
                <DialogContent>
                    <TextField
                        autoFocus
                        margin="normal"
                        id="gender"
                        label="Enter spot Id"
                        fullWidth
                        inputLabelProps={{shrink: true}}
                        value={value}
                        onChange={(e)=>setValue( e.target.value)}
                    />
                </DialogContent>
                <DialogActions>
                    <Button color={"inherit"} onClick={closeDialog}>Cancel</Button>
                    <Button type={"submit"} variant={"contained"} color="primary">
                        Start parking
                    </Button>
                </DialogActions>
            </form>
        </Dialog>
        </>);
};

export default EnterId;
