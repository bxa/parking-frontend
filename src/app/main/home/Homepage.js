// import { BaiduMap, Marker } from 'react-baidu-maps';
import MainToolbar from './MainToolbar';
import ParkingList from "../../lots/ParkingList";
import {Link} from "react-router-dom";

import {Button, Card, CardContent, CardHeader, Divider, Typography} from '@mui/material';
import {useLocation} from "react-router";

const HomePage  = (props) => {
    const {lots} = props;
    let location = useLocation();
    const parkingTitle = lots?.length > 0 ?
        <Typography variant={"subtitle1"}> <span> {lots?.length}</span>
            Parking lots found!
        </Typography> : <Typography >Nothing found</Typography>;
    return (
        <>

            <MainToolbar/>
            <Card className={"mt-14"}>
                <CardContent>
                    <Typography   variant="h6">
                        Add your first vehicle
                    </Typography>
                    <Button component={Link} to={"/user/vehicles/add"} variant="contained" className={"mt-4"} fullWidth size={"large"} color="primary">Add vehicle</Button>
                </CardContent>
            </Card>
            <Divider light/>
            <Card>
                <CardHeader title={parkingTitle}>
                </CardHeader>
                <CardContent className={"bg-blue-400 p-12 h-64"}>
                    {/*<BaiduMap mapContainer={<div style={{ height: '200px' }} />}>*/}
                    {/*<Marker position={{ lng: 11576266, lat: 3581455 }} />*/}
                    {/*</BaiduMap>*/}
                </CardContent>
            </Card>
            <Card>
                <Typography variant="subtitle1" className={"mt-8"}>
                    Explore
                </Typography>
                <ParkingList/>
            </Card>
        </>
    );
};

export default HomePage;
