import useSWR from "swr"

export function useLots() {
    const { data, error, mutate} = useSWR('/lots');

    return {
        lots: data?.result,
        isLoading: !error && !data,
        isError: error,
        refresh: mutate,
    }
}

