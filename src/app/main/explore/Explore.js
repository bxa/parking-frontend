import ParkingList from "../../lots/ParkingList";
import Card from "@mui/material/Card";
import CardHeader from "@mui/material/CardHeader";
import MainToolbar from "../home/MainToolbar";
import {useState} from "react";
import Marker from "react-amap-binding/es/Marker";
import InfoWindow from "react-amap-binding/es/InfoWindow";
import AMap from "react-amap-binding/es/AMap";
import IconButton from "@mui/material/IconButton";
import {Expand, Minimize} from "@mui/icons-material";

const Explore  = () =>{
    const pos = [103.901465 ,30.795899 ];
    const infoWindow = {
        isCustom: false,
        position: [103.901465 ,30.795899 ],
        content: '<div>InfoWindow</div>',
        autoMove: true,
        offset: [0, 0],
        visible: true,
        size: [120, 50],
    };
    const [mini, setMini] = useState(false);

    return (
        <>
            <MainToolbar/>
            <div style={{height: mini ? "calc(100vh - 260px)" : "40vh"}} className={"mt-12 bg-blue-200"}>
                <AMap resizeEnabled appKey={"db41b38ab7028e90c33f2baa9e1330ab"}  zoom={16}
                      center={pos} >

                    <Marker
                        position={pos}>
                    </Marker>
                    <InfoWindow
                        {...infoWindow}
                        // onClose={this.handleClick}
                    />
                </AMap>
            </div>
            <Card style={{zIndex: 999, height: mini ? "160px" : "50vh"}} className={"fixed bottom-16 left-2 right-2  overflow-y-scroll"}>
                <CardHeader  title={"Parking lots"} action={<IconButton className={"mr-4"} onClick={()=>setMini(!mini)}>{mini ? <Expand /> : <Minimize />}</IconButton>}/>
                <ParkingList />
            </Card>
        </>
    );
};

export default Explore;
