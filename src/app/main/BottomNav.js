import {BottomNavigation, BottomNavigationAction, Paper} from '@mui/material';
import {Link, matchPath, Outlet, useLocation} from "react-router-dom";
import {AccountCircle, BusAlert, Explore, LocalParking} from "@mui/icons-material";
import {roles, useAuth} from "../../auth/AuthProvider";

const styles = {
    button: {
        margin: 'auto',
        display: 'block',
        marginTop: 20
    },
    input: {
        display: 'none',
    },
    head: {
        marginTop: 20
    },
    icon: {
        flexDirection: "column"
    },
    bottomNav: {
        position: 'fixed',
        bottom: 0,
        width: '100%',
        borderTop: '1px solid #fafafa',
        height: 60,
        background: 'white',
        zIndex: 100

    },
    tab: {
        height: 59
    },
    labelWrapped: {
        paddingBottom: 0
    },
    tabSelected: {
        color: 'black',
        '& $label': {fontWeight: 'bold'}
    },
    container: {
        // marginBottom: 60,
        overflow: "scroll",
        height: "calc(100vh - 60px)",
    },
    containerWrap: {
        // marginBottom: 60,
        overflow: "hidden",
        height: "calc(100vh - 60px)",
    }
};

function useRouteMatch(patterns) {
    const { pathname } = useLocation();

    for (let i = 0; i < patterns.length; i += 1) {
        const pattern = patterns[i];
        const possibleMatch = matchPath(pattern, pathname);
        if (possibleMatch !== null) {
            return possibleMatch;
        }
    }

    return null;
}

const BottomNav = () =>  {
    // componentDidMount() {
    //     if(props.location.pathname === "/explore") setState({value: 1});
    //     else if(props.location.pathname === "/profile") setState({value: 2});
    //     // axios.get("http://googsell.loc:8080/admin/lots/json",{
    //     //     headers: {
    //     //         'Access-Control-Allow-Origin': '*',
    //     //         'Content-Type': 'application/json',
    //     //     }
    //     // }).then(response => {
    //     //     setState({lots: response.data});
    //     // })
    //     // axios.get("http://lorawan.timeddd.com/intf/data.ashx?ecode=1703&key=xi-nan-jiao-tong-da-xue-123-456-789")
    //     //     .then(response => {
    //     //     setState({lots: response.data.dataList});
    //     // })
    // }

    const {role} = useAuth();

    const routeMatch = useRouteMatch(['/', '/explore', '/profile', '/myLot', '/anomalies']);
    const currentTab = routeMatch?.pattern?.path;
    return (
            <div>
                <Outlet />
                <Paper sx={{ position: 'fixed', bottom: 0, left: 0, right: 0 }} elevation={3}>
                <BottomNavigation
                    showLabels
                    value={currentTab}
                >
                    {role === roles.operator && <BottomNavigationAction  component={Link} to={"/anomalies"} value={"/anomalies"} label="Anomalies" icon={<BusAlert />} />}
                    {role === roles.operator && <BottomNavigationAction  component={Link} to={"/myLot"} value={"/myLot"} label="My Parking" icon={<LocalParking />} />}
                    {/*<BottomNavigationAction  component={Link} to={"/"} value={"/"} label="Home" icon={<Icon>home</Icon>} />*/}
                    {role === roles.driver && <BottomNavigationAction  component={Link} to={"/"} value={"/"} label="Explore" icon={<Explore />} />}
                    <BottomNavigationAction  component={Link} to={"/profile"} value={"/profile"} label="My profile" icon={<AccountCircle />} />
                </BottomNavigation>
                </Paper>
            </div>
        )
};

export default BottomNav;
