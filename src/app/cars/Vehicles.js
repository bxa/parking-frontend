import {Link} from "react-router-dom";
import {
    Button, Card, CardContent, IconButton, List, ListItem, ListItemSecondaryAction, ListItemText, Typography,
} from '@mui/material';
import useSWR from "swr";
import Loading from "../../sharedComponents/Loading";
import Error500Page from "../../sharedComponents/Error500Page";
import {makeStyles} from "@mui/styles";
import clsx from "clsx";
import {Add, Delete, Edit} from "@mui/icons-material";


const styles = makeStyles(theme => ({
    button: {
        border: '3px dashed #ccc',
        width: '100%',
        height: 80,
        marginTop: 20,
        textAlign: 'center',
        lineHeight: '45px',

    }
}));

const Vehicles  = (props) => {
    const {data, error} = useSWR('/profile/cars');
    const classes = styles();

    if (!data && !error) return <Loading />;

    return (
        <Card square>

                <CardContent >
                    <Typography variant={"subtitle1"}>
                        You can add up to 3 vehicles
                    </Typography>
                    <Button startIcon={<Add />} component={Link} to={"/profile/vehicles/add"} size={"large"} className={clsx(classes.button)} color={"primary"}>
                        <Typography variant={"h6"} color={"inherit"}>Add Vehicle</Typography>
                    </Button>
                </CardContent>
            {
                (error) ? <Error500Page /> : <List className={"bg-white"}>
                    {data?.map((_i, i) =>
                        <ListItem key={i}>
                            <div className={"rounded h-5 w-5 mr-4"} style={{backgroundColor: _i.color}}/>
                            <ListItemText
                                primary={_i.brand}
                                secondary={_i.plateNumber}
                            />
                            <ListItemSecondaryAction>
                                <IconButton component={Link} to={`/car/${_i.id}`}>
                                    <Edit />
                                </IconButton>
                                <IconButton onClick={()=>{
                                    // deleteConfirm("/car", _i.id, _i.brand + " (" + _i.plate + ")")
                                }}
                                >
                                    <Delete />
                                </IconButton>
                            </ListItemSecondaryAction>
                        </ListItem>
                    )}
                </List>
            }
        </Card>
    );
};

export default Vehicles;

