import axios from "../../myaxios"

import {Button, Card, CardContent, Checkbox, FormControlLabel, Icon, TextField, Typography,} from '@mui/material';
import {useState} from "react";
import {useNavigate, useParams} from "react-router-dom";
import clsx from "clsx";
import {makeStyles} from "@mui/styles";
import Loading from "../../sharedComponents/Loading";
import {useMessage} from "../../context/messageContext/MessageProvider";
//
// const styles = theme => ({
//     button: {
//         display: 'block',
//         width: '60%',
//         margin: 'auto',
//         height: 50,
//         marginTop: 40,
//     },
//     header: {
//         margin: 20
//     },
//     bootstrapRoot: {
//         padding: 0,
//         width: '100%',
//         // background: 'white',
//         'label + &': {
//             marginTop: theme.spacing.unit * 3,
//         },
//     },
//     bootstrapInput: {
//         border: '1px solid '+theme.palette.primary.main+'  !important',
//         borderRadius: 3,
//         fontSize: "20px !important",
//         background: 'white !important' ,
//         padding: '5px 15px  !important',
//         width: 'calc(100% - 24px)',
//         height: 50,
//     },
//     bootstrapFormLabel: {
//         fontSize: 18,
//     },
//     input:{
//         marginTop: 25
//     }
// });

const styles = makeStyles(theme => ({
    selected: {
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.getContrastText(theme.palette.primary.main)
    }
}));

const colors = ["white",
    "gray",
    "black",
    "blue",
    "green",
    "red",
];

const AddVehicle  = () => {
    const m = useMessage();
    const classes = styles();
    const {carId} = useParams();
    const navigate = useNavigate();
    const [step, setStep] = useState(0);
    const [checkedA, setCheckedA] = useState(false);
    const [loading, setLoading] = useState(false);
    const [formData, setFormData] = useState({
        plate: "",
        brand: "",
        modelYear: "",
        color: "",
    });
    const [error, setError] = useState(null);

    const handleCheck = event => {
        setCheckedA(event.target.checked );
    };

    const handleChange = name => event => {
        setFormData(formData => ({...formData, [name]: event.target.value }));
    };

    const onSubmit = () => {
        console.log(m);
        setLoading(true);
        const request = (carId) ? axios.put('/profile/cars', {...formData, id: carId}) : axios.post('/profile/cars', formData);
        request.then(() => {
            m?.showMessage("Vehicle just added!", "success");
            navigate(-1);
        }).finally(()=>setLoading(false))
    };
    if (loading) return <Loading />;
    return (
        <Card square>
            <CardContent>
                {!step ?
                    <>
                        <Typography variant="h6" className={'text-gray-500 font-bold mb-4'} >
                            Add your plate number carefully
                        </Typography>
                        <TextField
                            onChange={handleChange('plate')}
                            fullWidth
                            value={formData.plate}
                            id="bootstrap-input"
                            placeholder={"川A-A34X93"}
                            label={"Plate Number"}
                            InputLabelProps={{
                               shrink: true,
                               // className: classes.bootstrapFormLabel,
                            }}>
                        </TextField>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    checked={checkedA}
                                    onChange={handleCheck}
                                    value="checkedA" color="primary"
                                />
                            }
                            label="I accept terms and conditions"
                        />
                        {!!error && <div className={"bg-red-500 p-3 border-red-500 text-white rounded mb-5 flex items-center"}><Icon className={"mr-3"}>error</Icon> {error}</div>}
                        <Button fullWidth onClick={()=>setStep(1)}
                                disabled={!checkedA || formData.plate.length < 5} size={"large"}
                                // className={classes.button}
                                variant={"contained"} color={"primary"}>Next Step</Button>
                    </>
                    :
                    <>
                        <Typography variant="h6" className={"text-gray-500 font-bold mb-4"} >
                            Add your other information
                        </Typography>
                        <div >
                            <TextField
                                label={"Brand"}
                                className={"mb-4"}
                                onChange={handleChange('brand')}
                                fullWidth
                                value={formData.brand}
                                placeholder={"Brand"}/>
                            <TextField
                                label={"Year"}
                                className={"mb-4"}
                                type={"number"}
                                inputProps={{
                                    step: 1,
                                    type:"number",
                                    length: 4,
                                    min: 1980,
                                    max: 2022
                                }}
                                onChange={handleChange('modelYear')}
                                fullWidth
                                value={formData.modelYear}
                                placeholder={"Year"}/>
                                <div className={""}>
                                    <Typography>Color</Typography>
                                    <span className={"bg-blue-500 bg-red-500 bg-green-500 bg-black bg-white bg-gray-500 hidden"}/>
                                    {colors.map(c => <button key={c} onClick={()=>handleChange('color')({target: {value: c}})} className={clsx(formData.color === c && classes.selected," p-2 capitalize m-2 border-solid rounded border border-1 border-gray-200 bg-gray-100")}><span className={`bg-${c} border-slate-300 border border-solid  align-text-top bg-${c}-500 w-4 h-4 inline-block rounded`}/> {c}</button>)}
                                </div>
                            <Button fullWidth onClick={onSubmit}
                                    disabled={!checkedA || formData.brand === ""} size={"large"}
                                    variant={"contained"} color={"primary"}>Next Step</Button>
                        </div>
                    </>
                }
            </CardContent>
        </Card>
    );
};

export default AddVehicle;
