import {
    Avatar,
    Button,
    Card,
    CardContent,
    CardHeader,
    Grid,
    Icon,
    IconButton,
    List,
    ListItem,
    ListItemSecondaryAction,
    ListItemText,
    Typography,
} from '@mui/material';
import {useLot} from "./useLot";
import Loading from "../../sharedComponents/Loading";
import Error500Page from "../../sharedComponents/Error500Page";
import {useNavigate} from "react-router-dom";
import {Directions, Navigation} from "@mui/icons-material";
import Marker from "react-amap-binding/es/Marker";
import InfoWindow from "react-amap-binding/es/InfoWindow";
import AMap from "react-amap-binding/es/AMap";


// const useStyles = makeStyles( ()=>({
//     icon: {
//         flexDirection: "column"
//     },
//     list: {
//         background: 'white'
//     },
//     icons: {
//         fontSize: '2rem'
//     },
//     caption: {
//         marginTop: 10,
//         marginBottom: 20
//     },
//     title: {
//         fontSize: '1.7em'
//     },
//     avatar: {
//         background: "#eee"
//     }
// }));

const SingleLot  = (props) => {
    const {lot, isLoading, isError} = useLot();
    const navigate = useNavigate();

    const toStart = () => {
        // todo navigate to the parking lot
        // navigate("/start");
    };

    const pos = [103.901465 ,30.795899 ];
    const infoWindow = {
        isCustom: false,
        position: [103.901465 ,30.795899 ],
        content: '<div>InfoWindow</div>',
        autoMove: true,
        offset: [0, 0],
        visible: true,
        size: [120, 50],
    };

    if (isLoading) return <Loading />;
    if (isError) return <Error500Page />;
    return (
        <div>
            <Card  className={"fixed top-2 left-2 right-2 overflow-y-scroll"}>
                <List dense component="nav">
                    <ListItem>
                        <ListItemText primary={lot.name} secondary={lot.capacity + " parking spot available"}/>
                        <ListItemSecondaryAction>
                            <IconButton
                                aria-label="Delete"
                                color={"default"}
                                size="large">
                                <Directions /> <span className="span">{lot.distanse}</span>
                            </IconButton>
                        </ListItemSecondaryAction>
                    </ListItem>
                </List>
            </Card>
            <div className={"h-screen bg-blue-200"}>
                <AMap resizeEnabled appKey={"db41b38ab7028e90c33f2baa9e1330ab"}  zoom={16}
                      center={pos} >

                    <Marker
                        position={pos}>
                    </Marker>
                    <InfoWindow
                        {...infoWindow}
                        // onClose={this.handleClick}
                    />
                </AMap>
            </div>
            <Card className={"fixed bottom-16 left-2 right-2 "}>
                <CardHeader avatar={
                    <Avatar aria-label="Recipe">
                        <Icon color={"primary"}>place</Icon>
                    </Avatar>
                } action={
                    <IconButton onClick={toStart} color={"primary"} size="large">
                        <Navigation />
                    </IconButton>
                } subheader={lot.address?.fullAddress} title={"Address"}/>
                <CardContent>
                    <Grid container className="tc mb-2">
                        <Grid item xs={4}>
                            <Typography variant={"h5"} color={"primary"}>{lot.emptySpace}</Typography>
                            <Typography variant={"caption"}>Empty spaces</Typography>
                        </Grid>
                        <Grid item xs={4}>
                            <Typography variant={"h5"} >{lot.capacity}</Typography>
                            <Typography variant={"caption"}>Spaces</Typography>
                        </Grid>
                        <Grid item xs={4}>
                            <Typography variant={"h5"} >{lot.chance}%</Typography>
                            <Typography variant={"caption"}>Your chance</Typography>
                        </Grid>
                    </Grid>
                    <Button size={"large"} className={"mt-2"} onClick={toStart} variant={"contained"} color={"primary"} fullWidth><Navigation /> Navigate</Button>

                </CardContent>
            </Card>
        </div>
    );
};

export default SingleLot;
