import {Link} from "react-router-dom";
import {IconButton, List, ListItem, ListItemSecondaryAction, ListItemText} from '@mui/material';
import {useLots} from "../main/useLots";
import Loading from "../../sharedComponents/Loading";
import Error500Page from "../../sharedComponents/Error500Page";
import Chip from "@mui/material/Chip";
import {Directions} from "@mui/icons-material";

const ParkingList = () => {
    const {lots, isError, isLoading} = useLots();

    if (isLoading) return <Loading />;
    if (isError) return <Error500Page />;
    return (
        <List >
            {lots?.map(_lot =>
                <ListItem component={Link} to={`/lot/${_lot.id}`} button key={_lot.id} divider>
                    <ListItemText  primary={<><Chip size={"small"} color={"primary"} label={_lot.capacity + " spots"}/> {_lot.name} </>}  secondary={_lot.address?.fullAddress }/>
                    <ListItemSecondaryAction>
                        <IconButton
                            aria-label="Delete"
                            color={"primary"}
                            size="large">
                            <Directions fontSize={"large"}/> <span className="span">{_lot.distance}</span>
                        </IconButton>
                    </ListItemSecondaryAction>
                </ListItem>)
            }
        </List>
    );
};

export default ParkingList;
