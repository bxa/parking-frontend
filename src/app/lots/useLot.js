import useSWR from 'swr'
import {useParams} from "react-router-dom";

export function useLot() {
    const {id} = useParams();
    const { data, error, mutate} = useSWR(id ? `/lots/${id}` : null);

    return {
        lot: data,
        isLoading: !error && !data,
        isError: error,
        refresh: mutate,
    }
}
