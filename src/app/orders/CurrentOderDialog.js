import {Button, Dialog, DialogActions, DialogTitle} from "@mui/material";
import {Link} from "react-router-dom";
import {useLastOrder} from "./useLastOrder";
import Loading from "../../sharedComponents/Loading";
import Error500Page from "../../sharedComponents/Error500Page";

const CurrentOrderDialog = ({closeDialog}) => {
    const {order, isLoading, error} = useLastOrder();

    if (isLoading) return <Loading />;
    if (error) return;
    return (<>
        <Dialog
            open={true}
            onClose={closeDialog}
            fullWidth
        >
            <DialogTitle id="form-dialog-title">
                {order?.status === "PROCESS" ? "Your car is parked." : "You have unpaid order"}
            </DialogTitle>

            <DialogActions>
                <Button color={"inherit"} onClick={closeDialog}>Cancel</Button>
                <Button component={Link} to={order?.status === "PROCESS" ?  "/start" : "/checkout"} variant={"contained"} color="primary">
                    Go to detail
                </Button>
            </DialogActions>
        </Dialog>
    </>);
};

export default CurrentOrderDialog;
