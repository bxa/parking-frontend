// import Timer from "./Timer/Timer"
// import Checkout from "./Checkout";
import {
    Button, Card, CardContent, List, ListItem, ListItemSecondaryAction, ListItemText, Typography,
} from '@mui/material';
import {makeStyles} from "@mui/styles";
import {useState} from "react";
import ReportDialog from "./ReportDialog";
import {Directions} from "@mui/icons-material";
import clsx from "clsx";
import moment from "moment"
import {secToDur} from "../profile/History";
import Loading from "../../sharedComponents/Loading";
import Error500Page from "../../sharedComponents/Error500Page";
import {useLastOrder} from "./useLastOrder";
import axios from "../../myaxios";
import {useNavigate} from "react-router-dom";

//
const styles = makeStyles(theme => ({
    colorDisabled: {
        // color: Gray["600"],
        fontWeight: 'lighter',
        display: 'inline-block',
        verticalAlign: 'middle',
        marginRight: 7
    },
    wrapper: {
        backgroundColor: theme.palette.primary.main
    },
    circle: {
        width: "55vw",
        height: "55vw",
        marginBottom: 20,
        background: 'white',
        border: "10px solid rgba(255,255,255,0.5)",
        borderRadius: "50%",
        backgroundClip: 'padding-box',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
    }
}));

const StartTime  = ({operator}) => {

    const {order, isLoading, error} = useLastOrder();
    //
    //
    // const {
    //     seconds,
    //     minutes,
    //     hours,
    //     days,
    //     isRunning,
    //     start,
    //     pause,
    //     resume,
    //     restart,
    // } = useStopwatch({ autoStart: false});
    const classes = styles();
    const [loading, setLoading] = useState(false);
    const [report, setReport] = useState(false);
    // const [event, setEvent] = useState(null);

    // const stopwatchOffset = new Date();
    // stopwatchOffset.setSeconds(stopwatchOffset.getSeconds() + 300)
    // const finishTime = () => {
    //     axios.put("/events/end").then(res => {
    //
    //     });
    // };

    // const spotId = "123123";

    // const startTime = () =>{
    //     setLoading(true);
    //     axios.post("/events/start", {spotId}).then(res => {
    //         // setStopwatchOffset(new Date(s));
    //         // start();
    //         setEvent(res.data);
    //     }).finally(()=>setLoading(false));
    // };

    // useEffect(()=>{
    //     startTime();
    // } ,[]);

    if (isLoading) return <Loading />;
    if (error) return <Error500Page />;
    return (
        <>
            <Card  square>
                <CardContent className={clsx("flex justify-center py-12 text-center", classes.wrapper)}>
                    <div className={classes.circle}>
                        {/*<Typography color={"textSecondary"}>Your park time</Typography>*/}
                        <Typography color={"textSecondary"} variant={"h6"}>Start: {moment(order.startDate).format("h:mma")}</Typography>
                        <Typography variant={"h3"} className={"font-bold"}>
                            {secToDur(order.duration)}
                                {/*<span>{days}</span>:<span>{hours}</span>:<span>{minutes}</span>:<span>{seconds}</span>*/}
                        </Typography>

                        { <Button color={"primary"} className={"capitalize"} variant={"text"}
                                              onClick={() => setReport(true)}>
                            Report a problem
                        </Button>}

                    </div>
                </CardContent>
                <List >
                    <ListItem>
                        <ListItemText primary={"Parking"}/>
                        <ListItemSecondaryAction>
                           {order.parkingName}
                        </ListItemSecondaryAction>
                    </ListItem>
                    <ListItem>
                        <ListItemText primary={"Address"}/>
                        <ListItemSecondaryAction>
                            <Button
                                endIcon={<Directions />}
                                color={"primary"}
                                >
                                {order.address}
                            </Button>

                        </ListItemSecondaryAction>
                    </ListItem>
                    <ListItem>
                        <ListItemText primary={"Price"}/>
                        <ListItemSecondaryAction>
                            <Typography color="primary" variant={"h6"}>
                                {order.price}<small className={"text-gray-400"}>RMB</small>
                            </Typography>
                        </ListItemSecondaryAction>
                    </ListItem>
                    {/*<ListItem  onClick={()=>setReport(true)} button >*/}
                    {/*    <ListItemIcon><Warning /></ListItemIcon>*/}
                    {/*    <ListItemText >Report a problem</ListItemText>*/}
                    {/*    <ListItemSecondaryAction>*/}
                    {/*        <ChevronRight className={"mt-1"} />*/}
                    {/*    </ListItemSecondaryAction>*/}
                    {/*</ListItem>*/}
                    {/*<ListItem divider onClick={finishTime} button >*/}
                    {/*    <ListItemText >End the time</ListItemText>*/}
                    {/*    <ListItemSecondaryAction>*/}
                    {/*        <ChevronRight className={"mt-1"} />*/}
                    {/*    </ListItemSecondaryAction>*/}
                    {/*</ListItem>*/}
                </List>
            </Card>

            <div className="text-center py-5">
                <Button color={'error'} onClick={()=>setReport(true)}>Report a problem</Button>
            </div>

            {/*<div className="pt-6 flex justify-center">*/}
            {/*    <Button size={"large"} endIcon={<ChevronRight />} variant={"contained"} color={"primary"}>Checkout</Button>*/}
            {/*</div>*/}
            {/*<div className="mt-12 flex justify-center">*/}
            {/*<button onClick={finishTime} className={"w-48 border-solid border-8 border-red-300 text-white font-bold  h-48 rounded-full bg-red-500 text-4xl"}>*/}
            {/*    FINISH*/}
            {/*</button>*/}
            {/*</div>*/}

            {report && <ReportDialog closeDialog={()=>setReport(false)}/>}

            {/*{state.checkout && <Checkout /> }*/}
        </>
    );
};

export default StartTime;


