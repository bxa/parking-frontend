import {useState} from 'react';
import axios from "../../myaxios";
import {Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, TextField} from "@mui/material";
import {useMessage} from "../../context/messageContext/MessageProvider";

const ReportDialog = ({eventId, closeDialog}) => {
    const [description, setDescription] = useState('');
    const {showMessage} = useMessage();
    const [loading, setLoading] = useState(false);

    const onSubmit = (e) => {
        e.preventDefault();
        setLoading(true);
        axios.post(`/events/${eventId}/report`, {description}).then(()=>{
            showMessage("Report sent", "success");
            closeDialog();
        }).finally(()=>{
            setLoading(false);
        })
    };

    return (<Dialog
        open={true}
        onClose={closeDialog}
        aria-labelledby="form-dialog-title"
        fullWidth
    >
        <form onSubmit={onSubmit}>
            <DialogTitle id="form-dialog-title">Report a problem</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    Please describe the situation:
                </DialogContentText>
                <TextField
                    multiline rows={3}
                    autoFocus
                    margin="dense"
                    label="Description"
                    fullWidth
                    value={description}
                    onChange={(e)=>setDescription(e.target.value)}
                />
            </DialogContent>
            <DialogActions>
                <Button onClick={closeDialog}>Cancel</Button>
                <Button variant={"contained"} type={"submit"} color="primary">
                    Send
                </Button>
            </DialogActions>
        </form>
    </Dialog>);
};

export default ReportDialog;
