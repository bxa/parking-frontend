import useSWR from "swr"

export function useLastOrder() {
    const { data, error, mutate} = useSWR('/lastOrder');

    return {
        order: data,
        isLoading: !error && !data,
        error,
        refresh: mutate,
    }
}
