import {
    Button, Card, CardContent, CardHeader, Divider, FormControlLabel, Radio, RadioGroup, Typography,
} from '@mui/material';
import {makeStyles} from "@mui/styles";
import {useState} from "react";
import Loading from "../../sharedComponents/Loading";
import Error500Page from "../../sharedComponents/Error500Page";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import ListItemSecondaryAction from "@mui/material/ListItemSecondaryAction";
import {secToDur} from "../profile/History";
import {useLastOrder} from "./useLastOrder";

const styles = makeStyles(() => ({
    bottomNav: {
        position: 'fixed',
        bottom: 0,
        width: '100%'
    },
    root: {
        height: 25
    },
    price: {
        marginTop: 40,
        marginBottom: 40
    },
    group: {
        marginLeft: 20
    },
    cardContent: {
        "&:last-child": {
            paddingBottom: 16
        }
    }

}));

const Checkout  = () => {
    const classes = styles();
    const {order, isLoading, error} = useLastOrder();
    const [way, setWay] = useState("wechat");

    if (isLoading) return <Loading />;
    if (error) return <Error500Page />;

    return (
        <div>
            <Card >
                <CardHeader title={<Typography   variant={"subtitle1"} color="textSecondary">
                    Payment
                </Typography>}>

                </CardHeader>
                <Divider  light/>
                <CardContent className={classes.cardContent}>
                    <List disablePadding>

                        <ListItem>
                            <ListItemText primary={"Parking"}/>
                            <ListItemSecondaryAction>
                                {order.parkingName}
                            </ListItemSecondaryAction>
                        </ListItem>
                        <ListItem>
                            <ListItemText primary={"Address"}/>
                            <ListItemSecondaryAction>
                                {order.address}
                            </ListItemSecondaryAction>
                        </ListItem>
                        <ListItem>
                            <ListItemText primary={"Duration"}/>
                            <ListItemSecondaryAction>
                                <Typography color="secondary" variant={"h6"}>
                                    {secToDur(order.duration)}
                                </Typography>
                            </ListItemSecondaryAction>
                        </ListItem>
                        <ListItem>
                            <ListItemText primary={"Price"}/>
                            <ListItemSecondaryAction>
                                <Typography color="primary" variant={"h5"}>
                                    {order.price}<small className={"text-gray-400"}>RMB</small>
                                </Typography>
                            </ListItemSecondaryAction>
                        </ListItem>
                    </List>


                    {/*<div  className={classes.price} >*/}
                    {/*    <Typography   component={'div'} variant={"h3"} color={"primary"}>{order.price} RMB</Typography>*/}
                    {/*    */}
                    {/*    <Typography    component={'a'} color={"textSecondary"}>Report a problem <ChevronRight style={{fontSize: 15, verticalAlign: 'middle'}}/></Typography>*/}
                    {/*</div>*/}
                    <CardContent>
                    <RadioGroup
                        aria-label="gender"
                        name="gender2"
                        className={classes.group}
                        value={way}
                        onChange={e=>setWay(e.target.value)}
                    >
                        <FormControlLabel value="wechat" control={<Radio classes={{root: classes.root}} color="secondary" />} label="WeChat Payment" />
                        <FormControlLabel value="alipay" control={<Radio color="secondary" />} label="AliPay Payment" />
                    </RadioGroup>
                    </CardContent>
                    <Button size="large" variant={"contained"} fullWidth  color="primary">Pay {order.price} RMB</Button>
                </CardContent>
            </Card>
        </div>
    );
};

export default Checkout;
