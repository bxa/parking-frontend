import axios from "./myaxios";

export const GET = "[LOTS] GET";

export const getLots = () => {
    return dispatch => {
        axios.get("/search").then(res=> {
            // if code has sent
            dispatch({type: GET, payload:res.data});
        })
    };
};
