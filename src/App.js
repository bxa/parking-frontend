import BottomNav from './app/main/BottomNav';
import './App.css';
import {createTheme, StyledEngineProvider, ThemeProvider} from '@mui/material/styles';
import History from "./app/profile/History";
import {BrowserRouter, Route, Routes} from "react-router-dom";
import MyProfile from "./app/profile/MyProfile";
import AuthProvider, {RequireAuth, RequireOperator} from "./auth/AuthProvider";
import LoginPage from "./auth/LoginPage";
import Settings from "./app/profile/settings/Settings";
import AboutUs from "./app/profile/AboutUs";
import EditProfile from "./app/profile/settings/EditProfile";
import {swrFetcher} from "./myaxios";
import {SWRConfig} from "swr";
import Header from "./sharedComponents/Header";
import Payments from "./app/profile/Payments";
import Vehicles from "./app/cars/Vehicles";
import AddVehicle from "./app/cars/AddVehicle";
import Explore from "./app/main/explore/Explore";
import ErrorBoundary from "./ErrorBoundary";
import SingleLot from "./app/lots/SingleLot";
import MessageProvider from "./context/messageContext/MessageProvider";
import StartTime from "./app/orders/StartTime";
import Checkout from "./app/orders/Checkout";
import MyLot from "./app/operator/myLot/MyLot";
import Spots from "./app/operator/myLot/Spots";
import Events from "./app/operator/Events";
import Transactions from "./app/operator/Transactions";
import Anomalies from "./app/operator/anomalies/Anomalies";


const mainTheme = createTheme({
    typography: {
        "fontSize": 13,
        "fontWeightLight": 300,
        "fontWeightMedium": 500,
        "fontWeightBold": 700
    },
    palette: {
        primary: {
            main: '#48875a',
        },
    },
    overrides: {
        // MuiCard: {
        //     root: {
        //         '&.BodyCard': {
        //             background: '#f3f3f3',
        //         }
        //     }
        // },
        MuiTypography: {
            root: {
                '&.light': {
                    color: '#aaa',
                    fontWeight: 'lighter'
                }
            }
        },
        MuiInput: {
            input: {
                '&.big': {
                    height: 40,
                    fontSize: "2rem"
                }
            }
        },
        MuiDivider: {
            root: {
                '&.big': {
                    height: 10,
                }
            }
        },
        // MuiButton: {
        //     sizeLarge: {
        //         minHeight: 50
        //     }
        // }
    }
});

// const jss = create({
//     ...jssPreset(),
//     plugins       : [...jssPreset().plugins, jssExtend()],
//     insertionPoint: document.getElementById('jss-insertion-point'),
// });
//
// const generateClassName = createGenerateClassName();

const  App = () => {
    // {/*<DeleteConformation/>*/}
    // {/*<MessageBox />*/}
    //
    // {/*<Route path="/checkout" exact element={<Checkout />}/>*/}
    // {/*<Route path="/about" exact element={<AboutUs />}/>*/}

    // {/**/}
    // {/*<Route path="/user/history" exact element={<History />}/>*/}
    // {/*<Route path="/user/payments" exact element={<Payments />}/>*/}
    // {/*<Route path="/user/wallet" exact element={<Wallet />}/>*/}
    // {/*<Route path="/" element={<Layout />}/>*/}
    // noinspection JSXNamespaceValidation
    return (
        <ErrorBoundary>
            <AuthProvider>
                <BrowserRouter>
                    <SWRConfig
                        value={{
                            refreshInterval: 0, fetcher: swrFetcher,
                        }}
                    >
                            <StyledEngineProvider injectFirst>
                                <ThemeProvider theme={mainTheme}>
                                    <MessageProvider>
                                        <Routes>
                                            <Route path="/" element={<BottomNav/>}>
                                                {/*<Route path="/" element={<HomePage />}/>*/}
                                                <Route path="/" element={<Explore/>}/>
                                                <Route path="lot/:id" element={<SingleLot/>}/>
                                                <Route path="profile" element={<RequireAuth/>}>
                                                    <Route index element={<MyProfile/>}/>
                                                    <Route path="settings" element={<><Header title={"Settings"}/><Settings/></>}/>
                                                    <Route path="edit" element={<><Header title={"EditProfile"}/><EditProfile/></>}/>
                                                    <Route path="history" element={<><Header title={"History"}/><History/></>}/>
                                                    <Route path="payments" element={<><Header title={"Payments"}/><Payments/></>}/>
                                                    <Route path="vehicles">
                                                        <Route index element={<><Header title={"Vehicles"}/><Vehicles/></>}/>
                                                        <Route path="add" exact element={<><Header title={"Add Vehicles"}/><AddVehicle/></>}/>
                                                    </Route>
                                                </Route>
                                                <Route  element={<RequireAuth/>}>
                                                    <Route path="myLot"  element={<RequireOperator><MyLot /></RequireOperator>}/>
                                                    <Route path="anomalies"  element={<RequireOperator><Header title={"Anomalies"} noBack/><Anomalies /></RequireOperator>}/>
                                                </Route>

                                            </Route>
                                            <Route  element={<RequireAuth/>}>
                                                <Route path="operator" element={<RequireOperator/>}>
                                                    <Route path="lots/:id/spots"  element={<><Header title={"Spots"}/><Spots /></>}/>
                                                    <Route path="lots/:id/events"  element={<><Header title={"Events"}/><Events /></>}/>
                                                    <Route path="lots/:id/transactions"  element={<><Header title={"Transactions"}/><Transactions /></>}/>
                                                    <Route path="start"  element={<><Header title={"Start parking"}/><StartTime/></>}/>
                                                </Route>
                                                <Route path="/checkout" element={<><Header title={"Checkout"}/><Checkout /></>}/>
                                                <Route path="/start"  element={<><Header title={"Start your parking"}/><StartTime /></>}/>
                                            </Route>


                                            <Route path="/about" element={<AboutUs/>}/>
                                            {/*<Route path="/user/vehicles/add" exact element={<AddVehicle />}/>*/}
                                            {/*<Route path="/user/vehicles" exact element={<Vehicles />}/>*/}
                                            <Route path="login" element={<LoginPage/>}/>
                                        </Routes>
                                    </MessageProvider>
                                </ThemeProvider>
                            </StyledEngineProvider>
                    </SWRConfig>
                </BrowserRouter>
            </AuthProvider>
        </ErrorBoundary>
    );
};

export default App;
