import {createContext, useContext, useState} from 'react';
import {IconButton, Snackbar, SnackbarContent} from "@mui/material";
import {amber, blue, green, grey, red} from "@mui/material/colors";
import {CheckCircle, Close, ErrorOutline, Info, Warning} from "@mui/icons-material";
import {makeStyles} from "@mui/styles";

let MessageContext = createContext(null);

export function useMessage() {
    return useContext(MessageContext);
}

const useStyles = makeStyles(theme => ({
    success: {
        backgroundColor: green["600"],
        color          : '#FFFFFF'
    },
    error  : {
        backgroundColor: red[600],
        color          : '#FFFFFF'
    },
    info   : {
        backgroundColor: blue[600],
        color          : '#FFFFFF'
    },
    warning: {
        backgroundColor: amber[600],
        color          : '#FFFFFF'
    },
    default: {
        backgroundColor: grey[700],
        color          : '#FFFFFF'
    }
}));

const variantIcon = {
    success: <CheckCircle className="mr-8" color="inherit"/>,
    warning: <Warning className="mr-8" color="inherit"/>,
    error  : <ErrorOutline className="mr-8" color="inherit"/>,
    info   : <Info className="mr-8" color="inherit"/>
};

const MessageProvider = ({children}) => {
    const [options, setOptions] = useState(null);
    const classes = useStyles();

    const showMessage = (message, variant) => {
        setOptions({
            message,
            variant
        });
    };
    const hideMessage = () => {
        setOptions(null)
    };

    const value = { showMessage, hideMessage };

    return <MessageContext.Provider value={value}>
        {children}
        <Snackbar
            autoHideDuration={5000}
            open={Boolean(options)}
            onClose={hideMessage}
            ContentProps={{
                variant        : 'body2',
                headlineMapping: {
                    body1: 'div',
                    body2: 'div'
                }
            }}
        >
            <SnackbarContent

                className={classes[options?.variant]}
                message={
                    <div className="flex items-center">
                        {variantIcon[options?.variant] && (
                            variantIcon[options?.variant]
                        )}
                        {options?.message}
                    </div>
                }
                action={[
                    <IconButton
                        key="close"
                        aria-label="Close"
                        color="inherit"
                        onClick={hideMessage}
                        size="large">
                        <Close />
                    </IconButton>
                ]}
            />
        </Snackbar>
    </MessageContext.Provider>;
};

export default MessageProvider;
