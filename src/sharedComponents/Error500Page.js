import Typography from "@mui/material/Typography";

const Error500Page = () => {
    return (
        <div className={"flex flex-col flex-1 items-center justify-center p-16"}>
            <Typography variant="h1" color="inherit" className="font-medium mb-5">
                500
            </Typography>
            <Typography variant="h5" color="textSecondary" className="mb-4">
                Something is wrong
            </Typography>
            <Typography color="textSecondary" className="mb-14">
                Please try again later
            </Typography>
        </div>);
};

export default Error500Page;
