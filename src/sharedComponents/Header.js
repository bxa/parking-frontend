import {AppBar, IconButton, Toolbar, Typography} from "@mui/material";
import {useNavigate} from "react-router-dom";
import {ChevronLeft} from "@mui/icons-material";

const Header = ({title, noBack}) => {
    let navigate = useNavigate();
    return (
        <AppBar position="static">
            <Toolbar disableGutters>
                {!noBack && <IconButton onClick={()=>navigate(-1)} color="inherit" size="large">
                    <ChevronLeft />
                </IconButton>}
                <Typography variant="h6" color="inherit">
                    {title}
                </Typography>
            </Toolbar>
        </AppBar>
        );
};

export default Header;
