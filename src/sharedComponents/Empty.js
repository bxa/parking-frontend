import {Card, CardContent, Icon, Typography} from "@mui/material";

const Empty = ({title}) => {
    return (
        <div className={"py-3 rounded bg-gray-200 text-center"}>
            <>
                <Typography variant={"caption"} color={"textSecondary"}  > {title || "Nothing found"} </Typography>
            </>
        </div>
    );
};

export default Empty;
