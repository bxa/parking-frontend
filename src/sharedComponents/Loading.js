import clsx from "clsx";
import {LinearProgress, Typography,} from '@mui/material';

const Loading = ({className}) => {
    return (<div className={clsx("flex m-12 flex-1 flex-col items-center justify-center " ,className)}>
            <Typography className="text-lg mb-4" color="primary">Loading...</Typography>
            <LinearProgress className={"w-full"} color="primary"/>
        </div>
    );
};

export default Loading;
